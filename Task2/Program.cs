﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangles rectangle1 = new Rectangles(0, 0, 2, 4);//creation of the first rectangle and set axis X, axis Y, width, height
            Rectangles rectangle2 = new Rectangles(3, 1, 3, 6);//creation of the second rectangle and set axis X, axis Y, width, height
            Rectangles rectangle3 = new Rectangles();          //creation of the empty rectangle

            rectangle1.Show();
            rectangle1.Move(2, 2);                             //moving a rectangle on the axis X and Y
            rectangle1.Show();

            rectangle2.Show();
            rectangle2.ChangeSize(1, 2);                       //resize width and height
            rectangle2.Show();

            rectangle3.ContainsTwo(rectangle1, rectangle2);    //rectangle, that containing two rectangles
            rectangle3.Intersection(rectangle1, rectangle2);   //intersection of two rectangles

            Console.ReadLine();
        }
    }
}
