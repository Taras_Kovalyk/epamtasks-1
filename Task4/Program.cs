﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] firstMatrix = new int[,]                    //creation of the first matrix
            {
                { 9, 8, 7 },
                { 6, 5, 4 },
                { 3, 2, 1 }
            };

            int[,] secomdMatrix = new int[,]                    //creation of the second matrix
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 4 }
            };
            Matrix matrix1 = new Matrix();                      //creation of the instance

            matrix1.determinant(firstMatrix);                   // Calculation of the determinant of the matrix
            matrix1.Sum(firstMatrix, secomdMatrix);             //matrix addition
            matrix1.Subtraction(secomdMatrix, firstMatrix);     //matrix subtraction
            matrix1.Multiplication(firstMatrix, secomdMatrix);  //matrix multiplication
            Console.ReadLine();
        }
    }
}
